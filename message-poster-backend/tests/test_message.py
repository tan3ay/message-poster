import unittest
from unittest.mock import patch
from models.message import Message


class TestUserModel(unittest.TestCase):

    mockValue = (1, 2, "Message", "2024-03-26T17:27:30.211712")
    mock_multiple_msg = [
        (1, 2, "Message1", "2024-03-26T17:27:30.211712"),
        (2, 2, "Message2", "2024-03-26T17:27:30.211712"),
    ]

    @patch("db.sqlite3.connect")
    def test_create(self, mock_connect):
        mock_cursor = (
            mock_connect.return_value.__enter__.return_value.cursor.return_value
        )
        mock_cursor.execute.return_value.execute.return_value.fetchone.return_value = (
            self.mockValue
        )
        message = Message.createMessage(2, "Message")
        self.assertEqual(message.id, self.mockValue[0])
        self.assertEqual(message.user_id, self.mockValue[1])
        self.assertEqual(message.value, self.mockValue[2])
        self.assertEqual(message.updated_at, self.mockValue[3])

    @patch("db.sqlite3.connect")
    def test_get_message_by_user(self, mock_connect):
        mock_cursor = (
            mock_connect.return_value.__enter__.return_value.cursor.return_value
        )
        mock_cursor.execute.return_value.fetchall.return_value = self.mock_multiple_msg
        message = Message.getMessageByUser(2)
        self.assertEqual(message[0].id, self.mock_multiple_msg[0][0])
        self.assertEqual(message[0].user_id, self.mock_multiple_msg[0][1])
        self.assertEqual(message[0].value, self.mock_multiple_msg[0][2])

    @patch("db.sqlite3.connect")
    def test_get_message(self, mock_connect):
        mock_cursor = (
            mock_connect.return_value.__enter__.return_value.cursor.return_value
        )
        mock_cursor.execute.return_value.fetchone.return_value = self.mockValue
        message = Message.getMessage(1)
        self.assertEqual(message.id, self.mockValue[0])
        self.assertEqual(message.user_id, self.mockValue[1])
        self.assertEqual(message.value, self.mockValue[2])
        self.assertEqual(message.updated_at, self.mockValue[3])

    @patch("db.sqlite3.connect")
    def test_delete_message(self, mock_connect):
        # Item present
        mock_cursor = (
            mock_connect.return_value.__enter__.return_value.cursor.return_value
        )
        mock_cursor.execute.return_value.rowcount = 1
        deleted = Message.delete(1)
        self.assertEqual(deleted, True)
        # Item Not present
        mock_cursor.execute.return_value.rowcount = 0
        deleted = Message.delete(1)
        self.assertEqual(deleted, False)

    @patch("db.sqlite3.connect")
    def test_update_message(self, mock_connect):
        # Item present
        mock_cursor = (
            mock_connect.return_value.__enter__.return_value.cursor.return_value
        )
        mock_cursor.execute.return_value.rowcount = 1
        updated = Message.update(1, "")
        self.assertEqual(updated, True)
        # Item Not present
        mock_cursor.execute.return_value.rowcount = 0
        updated = Message.update(1, "")
        self.assertEqual(updated, False)
