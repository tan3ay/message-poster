from unittest.mock import patch
from models.users import Users


@patch("db.sqlite3.connect")
def test_get_by_email_when_user_present(mock_connect):
    mock_cursor = mock_connect.return_value.__enter__.return_value.cursor.return_value
    mock_cursor.execute.return_value.fetchone.return_value = (1, "test@gmail.com")
    item = Users.get_by_email("test@gmail.com")
    assert item.email == "test@gmail.com"
    assert item.id == 1


@patch("db.sqlite3.connect")
def test_get_by_email_when_user_not_present(mock_connect):
    mock_cursor = mock_connect.return_value.__enter__.return_value.cursor.return_value
    mock_cursor.execute.return_value.fetchone.return_value = None
    item = Users.get_by_email("test@gmail.com")
    assert item == None


@patch("db.sqlite3.connect")
def test_create_user(mock_connect):
    mock_cursor = mock_connect.return_value.__enter__.return_value.cursor.return_value
    mock_cursor.execute.return_value.execute.return_value.fetchone.return_value = (
        1,
        "test@gmail.com",
    )
    item = Users.create("test@gmail.com")
    assert item.email == "test@gmail.com"
    assert item.id == 1
