from http import HTTPStatus
import pytest
import json
from app import app
from unittest.mock import patch
from models.message import Message
from models.users import Users


@pytest.fixture
def client():
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client


def test_get_messages_for_user(client):
    with patch(
        "models.message.Message.getMessageByUser",
        return_value=[Message(1, 1, "Message", "2024-03-26T17:27:30.211712")],
    ):
        response = client.get("/api/messages?userId=2")
        data = json.loads(response.data)
        assert response.status_code == 200
        assert len(data) > 0
        assert data[0]["id"] == 1
        assert data[0]["value"] == "Message"
        assert data[0]["updated_at"] == "2024-03-26T17:27:30.211712"
        # Invalid userId
        response = client.get("/api/messages?userId=ds")
        assert response.status_code == 422


def test_post_messages(client):
    with patch(
        "models.message.Message.createMessage",
        return_value=Message(1, 1, "My message", "2024-03-26T17:27:30.211712"),
    ):
        mockData = {"value": "My message", "userId": "2"}
        response = client.post("/api/messages", json=mockData)

        data = json.loads(response.data)
        assert response.status_code == HTTPStatus.CREATED
        assert data["id"] == 1
        assert data["value"] == "My message"
        assert data["updated_at"] == "2024-03-26T17:27:30.211712"
        # Invalid userId
        mockInvalidUserIdData = {"value": "My message", "userId": "sds"}
        response = client.post("/api/messages", json=mockInvalidUserIdData)
        assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_delete_messages(client):
    with patch("models.message.Message.delete", return_value=True):
        response = client.delete("/api/messages/1")
        if isinstance(response.text, str):
            data = response.text.encode("utf-8").decode("unicode-escape")
        assert response.status_code == HTTPStatus.OK
        assert data == "Message deleted successfully"
        # Failed delete
    with patch("models.message.Message.delete", return_value=False):
        response = client.delete("/api/messages/1")
        if isinstance(response.text, str):
            data = response.text.encode("utf-8").decode("unicode-escape")
        assert response.status_code == HTTPStatus.NOT_FOUND
        assert data == "Message not found."


def test_put_messages(client):
    with patch("models.message.Message.update", return_value=True):
        mockData = {"value": "My message"}
        response = client.put("/api/messages/1", json=mockData)
        if isinstance(response.text, str):
            data = response.text.encode("utf-8").decode("unicode-escape")
        assert response.status_code == HTTPStatus.OK
        assert data == "Message updated successfully"
        # Failed update
    with patch("models.message.Message.update", return_value=False):
        response = client.put("/api/messages/1", json=mockData)
        if isinstance(response.text, str):
            data = response.text.encode("utf-8").decode("unicode-escape")
        assert response.status_code == HTTPStatus.NOT_FOUND
        assert data == "Message not found."


def test_put_user_when_user_not_present(client):
    with patch("models.users.Users.get_by_email", return_value=None):
        with patch(
            "models.users.Users.create", return_value=Users(1, "5utamnay@gmail.com")
        ) as create:
            mockData = {"email": "5utamnay@gmail.com"}
            response = client.put("/api/users", json=mockData)
            data = json.loads(response.data)
            create.assert_called_once()
            assert data["id"] == 1
            assert data["email"] == "5utamnay@gmail.com"


def test_put_user_when_user_present(client):
    with patch(
        "models.users.Users.get_by_email", return_value=Users(1, "5utamnay@gmail.com")
    ):
        with patch(
            "models.users.Users.create", return_value=Users(1, "5utamnay@gmail.com")
        ) as create:
            mockData = {"email": "5utamnay@gmail.com"}
            response = client.put("/api/users", json=mockData)
            data = json.loads(response.data)
            create.assert_not_called()
            assert data["id"] == 1
            assert data["email"] == "5utamnay@gmail.com"


def test_get_user_when_user_not_present(client):
    with patch("models.users.Users.get_by_email", return_value=None):
        mockData = {"email": "5utamnay@gmail.com"}
        response = client.get("/api/users", json=mockData)
        if isinstance(response.text, str):
            data = response.text.encode("utf-8").decode("unicode-escape")
        assert response.status_code == HTTPStatus.NOT_FOUND
        assert data == "User not found."


def test_get_user_when_user_present(client):
    with patch(
        "models.users.Users.get_by_email", return_value=Users(1, "5utamnay@gmail.com")
    ):
        mockData = {"email": "5utamnay@gmail.com"}
        response = client.get("/api/users", json=mockData)
        data = json.loads(response.data)
        assert response.status_code == HTTPStatus.OK
        assert data["id"] == 1
        assert data["email"] == "5utamnay@gmail.com"
