from db import db_connection


class Users:
    def __init__(self, id, email):
        self.id = id
        self.email = email

    @classmethod
    def create(cls, email):
        with db_connection() as conn:
            cursor = conn.cursor()
            create_user_sql = """ INSERT INTO user(id,email) values (?,?)"""
            cursor = cursor.execute(create_user_sql, (None, email))
            lastRow = str(cursor.lastrowid)
            cursor = cursor.execute("SELECT * FROM user WHERE ROWID=" + lastRow)
            row = cursor.fetchone()
            conn.commit()

            return cls(row[0], row[1])

    @classmethod
    def get_by_email(cls, email):
        with db_connection() as conn:
            cursor = conn.cursor()
            cursor = cursor.execute("""SELECT * from user WHERE email= ? """, (email,))
            row = cursor.fetchone()
            if row is not None:
                return cls(row[0], row[1])
            return None
