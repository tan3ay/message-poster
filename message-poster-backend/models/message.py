from db import db_connection
from datetime import datetime


class Message:
    def __init__(self, id, user_id, value, updated_at):
        self.id = id
        self.user_id = user_id
        self.value = value
        self.updated_at = updated_at

    @classmethod
    def getMessageByUser(cls, user_id):
        with db_connection() as conn:
            cursor = conn.cursor()
            get_user_message_query = """SELECT * FROM message where user_id = ?"""
            cursor = cursor.execute(get_user_message_query, (user_id,))
            return [cls(*row) for row in cursor.fetchall()]

    @classmethod
    def getMessage(cls, id):
        with db_connection() as conn:
            cursor = conn.cursor()
            get_user_message_query = """SELECT * FROM message where id = ?"""
            cursor = cursor.execute(get_user_message_query, (id,))
            row = cursor.fetchone()
            if row is not None:
                return cls(*row)
            return None

    @classmethod
    def update(cls, id, new_value):
        with db_connection() as conn:
            cursor = conn.cursor()
            update_by_id = """UPDATE message
            SET value = ?,
                updated_at = ?
            WHERE id = ?"""
            cursor = cursor.execute(
                update_by_id, (new_value, datetime.today().isoformat(), id)
            )
            conn.commit()
            if cursor.rowcount == 1:
                return True
            return False

    @classmethod
    def delete(cls, id):
        with db_connection() as conn:
            cursor = conn.cursor()
            delete_by_id = """DELETE from message WHERE id = ?"""
            cursor = cursor.execute(delete_by_id, (id,))
            conn.commit()
            if cursor.rowcount == 1:
                return True
            return False

    @classmethod
    def createMessage(cls, user_id, value):
        with db_connection() as conn:
            cursor = conn.cursor()
            create_sql = """ INSERT INTO message(id,user_id, value, updated_at) values (?,?,?,?)"""
            cursor = cursor.execute(
                create_sql, (None, user_id, value, datetime.today().isoformat())
            )
            lastRow = str(cursor.lastrowid)
            cursor = cursor.execute("SELECT * FROM message WHERE ROWID=" + lastRow)
            row = cursor.fetchone()
            conn.commit()
            return cls(*row)
