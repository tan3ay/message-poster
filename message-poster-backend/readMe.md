cd message-poster-backend

# Build docker image

docker build -t massage-poster-api .

# Run docker image

docker run --rm -p 5001:5000 massage-poster-api
