import sqlite3

DATABASE = "message_db.sqlite"


def db_connection():
    return sqlite3.connect(DATABASE)


def init_db():
    with db_connection() as conn:
        cursor = conn.cursor()
        user_table_create_sql_query = """ CREATE TABLE IF NOT EXISTS  user(
            id integer PRIMARY KEY, 
            email text NOT NULL UNIQUE
            )
        """
        cursor.execute(user_table_create_sql_query)

        message_table_create_sql_query = """ CREATE TABLE IF NOT EXISTS  message(
            id integer PRIMARY KEY, 
            user_id integer NOT NULL,
            value text NOT NULL,
            updated_at timestamp,
            FOREIGN KEY(user_id) REFERENCES user(id)
            )
        """
        cursor.execute(message_table_create_sql_query)


init_db()
