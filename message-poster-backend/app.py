from models.users import Users
from models.message import Message
from db import init_db
from flask import Flask, request, jsonify
from email_validator import validate_email, EmailNotValidError
from http import HTTPStatus

app = Flask(__name__)

if __name__ == "__main__":
    print("Running")
    init_db()  # Initialize the database
    app.run()

INFO_MESSAGE_NOT_FOUND = "Message not found."
INFO_USER_NOT_FOUND = "User not found."
ERROR_INVALID_PARAM = "Invalid parameter passed."


@app.route("/api/messages", methods=["GET", "POST"])
def messages():
    if request.method == "GET":
        user_id = request.args.get("userId")
        if user_id is None or not user_id.isdigit():
            return ERROR_INVALID_PARAM, HTTPStatus.UNPROCESSABLE_ENTITY
        messages = Message.getMessageByUser(user_id)
        return jsonify([msg.__dict__ for msg in messages]), HTTPStatus.OK

    if request.method == "POST":
        data = request.json
        value = data["value"]
        user_id = data["userId"]
        if user_id is None or not str(user_id).isdigit():
            return ERROR_INVALID_PARAM, HTTPStatus.UNPROCESSABLE_ENTITY
        new_meesage = Message.createMessage(user_id, value)
        return jsonify(new_meesage.__dict__), HTTPStatus.CREATED


@app.route("/api/messages/<int:id>", methods=["PUT", "DELETE"])
def single_messages(id):
    if request.method == "PUT":
        data = request.json
        new_value = data["value"]
        result = Message.update(id, new_value)
        return (
            ("Message updated successfully", HTTPStatus.OK)
            if result
            else (INFO_MESSAGE_NOT_FOUND, HTTPStatus.NOT_FOUND)
        )

    if request.method == "DELETE":
        result = Message.delete(id)
        return (
            ("Message deleted successfully", HTTPStatus.OK)
            if result
            else (INFO_MESSAGE_NOT_FOUND, HTTPStatus.NOT_FOUND)
        )


@app.route("/api/users", methods=["PUT", "GET"])
def users():
    if request.method == "PUT":
        data = request.json
        userEmail = data["email"]
        if userEmail is not None and check(userEmail) == "false":
            return ERROR_INVALID_PARAM, HTTPStatus.UNPROCESSABLE_ENTITY

        existingUser = Users.get_by_email(userEmail)
        if existingUser is not None:
            return jsonify(existingUser.__dict__)
        else:
            newUser = Users.create(userEmail)
            return jsonify(newUser.__dict__)

    if request.method == "GET":
        userEmail = request.args.get("email")
        if userEmail is not None and check(userEmail) == "false":
            return ERROR_INVALID_PARAM, HTTPStatus.UNPROCESSABLE_ENTITY

        existingUser = Users.get_by_email(userEmail)
        if existingUser is not None:
            return jsonify(existingUser.__dict__)
        return INFO_USER_NOT_FOUND, HTTPStatus.NOT_FOUND


def check(email):
    try:
        v = validate_email(email)
        return "true"
    except EmailNotValidError as e:
        return "false"
