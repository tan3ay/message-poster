import { useEffect, type FC } from 'react';
import PostSection from '../containers/PostSection';
import MessageList from '../containers/MessageList';
import Notification from '../containers/Notification';
import Header from '../components/Header';
import { useNavigate } from 'react-router-dom';
import { UserState } from '../state/reducers/userReducer';

const MessagePage: FC<{
  user: UserState;
  verifyUser: (arg: string) => void;
}> = ({ user, verifyUser }) => {
  const emailParam = new URLSearchParams(window.location.search).get('email');
  const navigate = useNavigate();

  useEffect(() => {
    const fetchdata = async () => {
      if (!emailParam) {
        navigate(`/`);
      } else {
        try {
          await verifyUser(emailParam);
        } catch (e) {
          navigate(`/`);
        }
      }
    };
    fetchdata();
  }, [emailParam, navigate, verifyUser]);
  return user.id ? (
    <>
      <Notification />
      <Header />
      <PostSection />
      <MessageList />
    </>
  ) : null;
};

export default MessagePage;
