import { useState, type FC, useCallback } from 'react';
import TextField from '@mui/material/TextField';

import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Header from '../components/Header';

const Form = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
  gap: 20px;
  width: 500px;
  margin: auto;
  margin-top: -40px;
  & .MuiButtonBase-root {
    width: 200px;
    align-self: flex-end;
  }
`;

const Login: FC<{ createUser: (arg: string) => void }> = ({ createUser }) => {
  const [text, setText] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const validateEmail = (email: string) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const onEnter = useCallback(
    async (email: string) => {
      try {
        if (validateEmail(email)) {
          await createUser(email);
          navigate(`/dashboard?email=${email}`);
        } else {
          setError('true');
        }
      } catch (e) {
        //TODO check email verification here
        setError((e as Error).message);
      }
    },
    [createUser, navigate]
  );

  return (
    <Form>
      <Header />
      <TextField
        error={Boolean(error)}
        label="Enter your email here"
        value={text}
        placeholder={error}
        onChange={(e) => {
          setText(e.target.value);
        }}
        helperText={Boolean(error) && 'Please enter a valid email address'}
      />
      <Button
        size="large"
        variant="contained"
        color="primary"
        disabled={!Boolean(text)}
        onClick={() => onEnter(text)}
      >
        Enter
      </Button>
    </Form>
  );
};

export default Login;
