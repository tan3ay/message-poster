import { useState } from 'react';
import TextField from '@mui/material/TextField';

import { Button } from '@mui/material';

import { styled } from '@mui/material/styles';
import { UserState } from '../state/reducers/userReducer';

const CustomizedTextField = styled(TextField)`
  width: 100%;
`;

const CustomizedButton = styled(Button)`
  width: 120px;
  height: 50px;
`;

const PostSetcion: React.FC<{ user: UserState; post: Function }> = ({
  user,
  post,
}) => {
  const [inputMessage, setInputMessage] = useState('');

  const handleClick = () => {
    post(user.id, inputMessage);
    setInputMessage('');
  };

  return (
    <div
      style={{
        display: 'flex',
        gap: '10px',
        marginBottom: '16px',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'column',
      }}
    >
      <CustomizedTextField
        multiline
        label="Add your message here...."
        minRows={3}
        value={inputMessage}
        onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
          setInputMessage(e.target.value);
        }}
      />
      <CustomizedButton
        variant="contained"
        onClick={handleClick}
        disabled={!Boolean(inputMessage)}
      >
        Post
      </CustomizedButton>
    </div>
  );
};

export default PostSetcion;
