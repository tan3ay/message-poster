import styled from "styled-components";

const Title = styled.h1`
  text-align: center;
  margin-top: 60px;
`;

function Header() {
  return <Title>Message Poster</Title>;
}

export default Header;
