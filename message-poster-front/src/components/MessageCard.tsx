import { useState } from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import { Message } from '../state/typings';
import { IconButton } from '@mui/material';

import { ModeEdit, Delete, Clear, Save } from '@mui/icons-material';
import moment from 'moment';

const CustomizedCard = styled(Card)`
  display: flex;
  flex-direction: column;
  :hover .MuiCardHeader-action {
    visibility: visible;
  }
  break-inside: avoid;
  margin: 32px 0;
  &:first-of-type {
    break-inside: avoid;
    margin: 0px 0;
  }
`;

const CustomizedCardHeader = styled(CardHeader)`
  text-align: left;
  font-size: 12px;
  & .MuiCardHeader-action {
    visibility: hidden;
  }
`;

const CustomizedCardContent = styled(CardContent)`
  overflow-y: auto;
  align-items: center;
  display: flex;
  justify-content: left;
  white-space: pre;
`;

type MessageCardProps = {
  message: Message;
  onSave: (msg: Message) => void;
  onDelete: (id: number) => void;
};

const MessageCard: React.FC<MessageCardProps> = ({
  message,
  onSave,
  onDelete,
}) => {
  const [isEdit, setEdit] = useState(false);
  const [editContent, setEditContent] = useState(message.value);
  return (
    <CustomizedCard>
      <CustomizedCardHeader
        title=""
        subheader={moment(
          new Date(message.updated_at.toLocaleString('en-US'))
        ).format('Do MMM hh:mm')}
        action={
          isEdit ? (
            <>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setEdit(false);
                  setEditContent(message.value);
                }}
              >
                <Clear />
              </IconButton>

              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  onSave({
                    id: message.id,
                    value: editContent,
                    updated_at: new Date(),
                  });
                  setEdit(false);
                }}
              >
                <Save />
              </IconButton>
            </>
          ) : (
            <>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setEdit(true);
                }}
              >
                <ModeEdit />
              </IconButton>

              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  onDelete(message.id!);
                }}
              >
                <Delete />
              </IconButton>
            </>
          )
        }
      />

      {isEdit ? (
        <CardContent>
          <TextField
            fullWidth
            label=""
            id="fullWidth"
            multiline
            value={editContent}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
              setEditContent(e.target.value)
            }
          />
        </CardContent>
      ) : (
        <CustomizedCardContent>
          <Typography variant="h6" color="textSecondary" component="p">
            {message.value}
          </Typography>
        </CustomizedCardContent>
      )}
    </CustomizedCard>
  );
};

export default MessageCard;
