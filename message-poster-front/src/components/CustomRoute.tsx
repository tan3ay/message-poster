import React from "react";
import { Route, Navigate } from "react-router-dom";

interface CustomRouteProps {
  component: React.ComponentType<any>;
}

const CustomRoute: React.FC<CustomRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const emailParam = new URLSearchParams(window.location.search).get("email");

  return (
    <Route
      {...rest}
      loader={(props) =>
        emailParam ? <Component {...props} /> : <Navigate to="/login" />
      }
    />
  );
};

export default CustomRoute;
