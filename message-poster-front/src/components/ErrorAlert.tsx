import { useEffect, type FC } from "react";
import Alert from "@mui/material/Alert";
import { styled } from "@mui/material/styles";

const CustomizedAlert = styled(Alert)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  margin: auto;
  width: 40%;
`;

const ErrorAlert: FC<{ error: string | null; clearError: () => void }> = ({
  error,
  clearError,
}) => {
  useEffect(() => {
    if (error) {
      setTimeout(() => clearError(), 4000);
    }
  }, [clearError, error]);

  return error ? (
    <CustomizedAlert severity="error">{error}</CustomizedAlert>
  ) : null;
};

export default ErrorAlert;
