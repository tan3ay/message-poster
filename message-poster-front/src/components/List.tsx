import { useEffect } from 'react';
import MessageCard from './MessageCard';
import { Message, User } from '../state/typings';
import styled from 'styled-components';
import { UserState } from '../state/reducers/userReducer';

const MessageList = styled.div`
  column-count: 2;
  column-gap: 32px;
`;

const List: React.FC<{
  list: Message[];
  user: UserState;
  fetchList: (arg: number) => void;
  updateMessage: (arg: Message) => void;
  deleteMessage: (arg: number) => void;
}> = ({ list, user, fetchList, updateMessage, deleteMessage }) => {
  useEffect(() => {
    if (user.id) fetchList(user.id);
  }, [fetchList]);

  return (
    <MessageList>
      {list.map((msg) => (
        <MessageCard
          key={msg.id!}
          message={msg}
          onDelete={deleteMessage}
          onSave={updateMessage}
        />
      ))}
    </MessageList>
  );
};

export default List;
