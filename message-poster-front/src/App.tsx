import EntryPage from './containers/EntryPage';
import MessagePage from './containers/MessagePage';
import { Route, Routes } from 'react-router-dom';

function App() {
  return (
    <Routes>
      <Route path="/" element={<EntryPage />} />
      <Route path="/dashboard" element={<MessagePage />} />
    </Routes>
  );
}

export default App;
