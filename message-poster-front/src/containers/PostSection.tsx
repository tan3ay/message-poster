import { connect } from 'react-redux';
import { postMessage } from '../state/action-creators';
import InputSection from '../components/InputSection';
import { AppDispatch, RootState } from '../state/store';

const mapStateToProps = (state: RootState) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  post: (userId: number, text: string) => dispatch(postMessage(userId, text)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputSection);
