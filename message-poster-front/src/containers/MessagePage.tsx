import { connect } from 'react-redux';
import MessagePoster from '../pages/MessagePoster';
import { AppDispatch, RootState } from '../state/store';
import { verifyUser } from '../state/action-creators';
const mapStateToProps = (state: RootState) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  verifyUser: (email: string) => dispatch(verifyUser(email)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagePoster);
