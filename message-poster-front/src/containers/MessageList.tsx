import { connect } from 'react-redux';
import List from '../components/List';
import {
  loadUserMessages,
  updateMessage,
  deleteMessage,
} from '../state/action-creators';
import { Message } from '../state/typings';
import { AppDispatch, RootState } from '../state/store';

const mapStateToProps = (state: RootState) => ({
  list: state.messageList,
  user: state.user,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  fetchList: (userId: number) => dispatch(loadUserMessages(userId)),
  updateMessage: (message: Message) => dispatch(updateMessage(message)),
  deleteMessage: (id: number) => dispatch(deleteMessage(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
