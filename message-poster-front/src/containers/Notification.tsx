import { connect } from "react-redux";
import ErrorAlert from "../components/ErrorAlert";
import { clearError } from "../state/action-creators";
import { AppDispatch, RootState } from "../state/store";

const mapStateToProps = (state: RootState) => ({
  error: state.error,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  clearError: () => dispatch(clearError()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorAlert);
