import { connect } from "react-redux";
import Login from "../pages/Login";
import { createUser } from "../state/action-creators";
import { AppDispatch } from "../state/store";

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  createUser: (email: string) => dispatch(createUser(email)),
});

export default connect(null, mapDispatchToProps)(Login);
