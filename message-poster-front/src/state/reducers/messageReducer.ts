import { Action, ActionType, Message } from '../typings';

let initialState: Message[] = [];
const reducer = (state: Message[] = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.POST_MESSAGE:
      if (action.payload) {
        state = [...state, action.payload];
      }
      return state;
    case ActionType.LOAD_MESSAGE:
      if (action.payload) {
        return [...action.payload];
      }
      return state;
    case ActionType.UPDATE_MESSAGE:
      if (action.payload) {
        return [
          ...state.map((item) =>
            item.id === action.payload.id
              ? { ...item, value: action.payload.value }
              : item
          ),
        ];
      }
      return state;
    case ActionType.DELETE_MESSAGE:
      if (action.payload) {
        return [...state.filter((item) => item.id !== action.payload)];
      }
      return state;
    default:
      return state;
  }
};

export default reducer;
