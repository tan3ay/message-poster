import { ActionType, Action } from "../typings";

let initialState: string | null = null;
const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.ERROR_ALERT:
      return action.payload;
    case ActionType.ERROR_RESET:
      return null;
    default:
      return state;
  }
};

export default reducer;
