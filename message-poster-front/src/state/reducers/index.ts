import { combineReducers } from "redux";
import messageReducer from "./messageReducer";
import errorReducer from "./errorReducer";
import userReducer from "./userReducer";

export interface Message {
  id?: number;
  value: string;
  time: Date;
}

export const reducers = combineReducers({
  messageList: messageReducer,
  error: errorReducer,
  user: userReducer,
});

export type State = ReturnType<typeof reducers>;
