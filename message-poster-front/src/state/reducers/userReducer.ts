import { Action, ActionType, User } from '../typings';

export type UserState =
  | User
  | {
      id: null;
      email: null;
    };

let initialState: UserState = {
  id: null,
  email: null,
};

const reducer = (state: UserState = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.ADD_USER:
    case ActionType.GET_USER:
      return { ...action.payload };
    default:
      return state;
  }
};

export default reducer;
