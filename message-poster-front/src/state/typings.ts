export enum ActionType {
  POST_MESSAGE = 'post_message',
  LOAD_MESSAGE = 'load_message',
  UPDATE_MESSAGE = 'update_message',
  DELETE_MESSAGE = 'delete_message',
  ERROR_ALERT = 'error_alert',
  ERROR_RESET = 'error_reset',
  ADD_USER = 'add_user',
  GET_USER = 'load_user',
}

export interface Message {
  id?: number;
  value: string;
  updated_at: Date;
}
export interface User {
  id: number;
  email: string;
}
interface PostAction {
  type: ActionType.POST_MESSAGE;
  payload: Message;
}

interface LoadMessageAction {
  type: ActionType.LOAD_MESSAGE;
  payload: Message[];
}

interface UpdateMessageAction {
  type: ActionType.UPDATE_MESSAGE;
  payload: Message;
}

interface DeleteMessageAction {
  type: ActionType.DELETE_MESSAGE;
  payload: number;
}

interface ErrorAlertAction {
  type: ActionType.ERROR_ALERT;
  payload: string;
}

interface ErrorResetAction {
  type: ActionType.ERROR_RESET;
}

interface AddUserAction {
  type: ActionType.ADD_USER;
  payload: User;
}

interface GetUserAction {
  type: ActionType.GET_USER;
  payload: User;
}

export type Action =
  | PostAction
  | LoadMessageAction
  | UpdateMessageAction
  | DeleteMessageAction
  | ErrorAlertAction
  | ErrorResetAction
  | AddUserAction
  | GetUserAction;
