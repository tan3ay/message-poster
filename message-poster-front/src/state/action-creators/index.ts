import { Action, ActionType, Message } from '../typings';
import { Dispatch } from 'redux';
import { MessageService } from '../../api/message-service';
import { UserService } from '../../api/user-service';
import { error } from 'console';

export const postMessage = (userId: number, message: string) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      const data = (await MessageService.addMessage(userId, message)).data;
      dispatch({
        type: ActionType.POST_MESSAGE,
        payload: {
          id: data.id,
          value: data.value,
          updated_at: data.updated_at,
        },
      });
    } catch (e) {
      console.error(e);
      dispatch({
        type: ActionType.ERROR_ALERT,
        payload: 'Failed to post the message. Try to refresh the page',
      });
    }
  };
};

export const loadUserMessages = (userId: number) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      const data = (await MessageService.getMessages(userId)).data;
      const userMessages: Message[] = data.map((item: any) => {
        return { id: item.id, value: item.value, updated_at: item.updated_at };
      });
      dispatch({
        type: ActionType.LOAD_MESSAGE,
        payload: userMessages,
      });
    } catch (e) {
      console.error(e);
      dispatch({
        type: ActionType.ERROR_ALERT,
        payload: 'Failed to load message list. Try to refresh the page',
      });
    }
  };
};

export const updateMessage = (message: Message) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      await MessageService.updateMessage(message);
      dispatch({
        type: ActionType.UPDATE_MESSAGE,
        payload: message,
      });
    } catch (e) {
      console.error(e);
      dispatch({
        type: ActionType.ERROR_ALERT,
        payload: 'Failed to update message',
      });
    }
  };
};

export const deleteMessage = (id: number) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      await MessageService.deleteMessage(id);
      dispatch({
        type: ActionType.DELETE_MESSAGE,
        payload: id,
      });
    } catch (e) {
      console.error(e);
      dispatch({
        type: ActionType.ERROR_ALERT,
        payload: 'Failed to delete message',
      });
    }
  };
};

export const clearError = () => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.ERROR_RESET,
    });
  };
};

export const createUser = (email: string) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      const result = await UserService.addUser(email);
      dispatch({
        type: ActionType.ADD_USER,
        payload: result.data,
      });
    } catch (e) {
      console.error(e);

      dispatch({
        type: ActionType.ERROR_ALERT,
        payload: 'Failed to create the user',
      });
    }
  };
};

export const verifyUser = (email: string) => {
  return async (dispatch: Dispatch<Action>) => {
    try {
      const result = await UserService.getUserByEmail(email);
      dispatch({
        type: ActionType.GET_USER,
        payload: result.data,
      });
    } catch (e) {
      console.error(e);
      dispatch({
        type: ActionType.LOAD_MESSAGE,
        payload: [],
      });
      // dispatch({
      //   type: ActionType.ERROR_ALERT,
      //   payload: 'Error occurred while loading user data',
      // });
      throw new Error('User not presesnt');
    }
  };
};
