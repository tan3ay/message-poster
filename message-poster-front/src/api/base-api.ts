import axios from 'axios';

const defaultHeader: Record<string, any> = {};

export const API = {
  baseURL: 'http://localhost:3000/api',

  getURL(URL: string): string {
    return this.baseURL + URL;
  },

  async makeGetRequest(URL: string, param: Record<string, any>) {
    return await axios.get(this.getURL(URL), {
      params: param,
    });
  },

  async makePostRequest(
    URL: string,
    requestBody: Record<string, any>,
    headers = defaultHeader
  ) {
    return await axios.post(this.getURL(URL), requestBody, headers);
  },

  async makePutRequest(
    URL: string,
    requestBody: Record<string, any>,
    headers = defaultHeader
  ) {
    return await axios.put(this.getURL(URL), requestBody, headers);
  },

  async makeDeleteRequest(URL: string, headers = defaultHeader) {
    return await axios.delete(this.getURL(URL), headers);
  },
};
