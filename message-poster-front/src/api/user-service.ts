import { API } from './base-api';

const MESSAGE_ENDPOINT = '/users';

export const UserService = {
  getUserByEmail(email: string) {
    return API.makeGetRequest(`${MESSAGE_ENDPOINT}?email=${email}`, {});
  },

  addUser(email: string) {
    const reqBody = {
      email,
    };
    return API.makePutRequest(MESSAGE_ENDPOINT, reqBody);
  },
};
