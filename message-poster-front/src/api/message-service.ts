import { Message } from './../state/typings';
import { API } from './base-api';

const MESSAGE_ENDPOINT = '/messages';

export const MessageService = {
  async getMessages(id: number) {
    const param = {
      userId: id,
    };
    return await API.makeGetRequest(MESSAGE_ENDPOINT, param);
  },

  addMessage(id: number, value: string) {
    const reqBody = {
      userId: id,
      value: value,
    };
    return API.makePostRequest(MESSAGE_ENDPOINT, reqBody);
  },

  updateMessage(message: Message) {
    return API.makePutRequest(`${MESSAGE_ENDPOINT}/${message.id}`, message);
  },

  deleteMessage(id: number) {
    return API.makeDeleteRequest(`${MESSAGE_ENDPOINT}/${id}`);
  },
};
