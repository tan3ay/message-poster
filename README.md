<h1># message-poster</h1>
<br />
You can test the message poster app using docker<br />
Steps<br />
<br />
1. Install docker<br />
2. Clone repo to your local machine<br />
3. cd message-poster<br />
4. Run command: docker-compose up<br />
5. Access app on http://localhost:3000<br />

<h2>Features</h2>
Flow:-<br /><br />
1. User Session <br />
&nbsp;- User can enter his email_id to click Enter<br />
&nbsp;- Application do not have auth features, so session is decided by user email_id<br /><br />
2. Message Poster Dashboard<br />
&nbsp;- User can write message in Text Area and click POST to post it<br />
&nbsp;- The posted message will be visible on the screen/wall<br />
&nbsp;- User can manage posted message on wall.<br />
        &nbsp;&nbsp;- To Edit/Delete any specific message<br />
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Move mouse cursor on the message card, you can see option to edit and delete on top right corner<br />
